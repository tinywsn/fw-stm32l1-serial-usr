# fw-stm32l1-serial-usr

#### 介绍
TinyWSN 是一套SUB-1G的无线传感器网络-http://www.tinywsn.net，
本项目是节点模块的外置处理器的使用示例-http://www.tinywsn.net/wordpress/index.php/node/

#### 软件架构
软件架构说明
本项目运行在STM32L151C8T6-A处理器的开发板上，可以自行从淘宝购买，它通过串口和节点模块进行消息收发，发送前它还检查节点模块的状态管脚STA_IND避免消息缓冲溢出


#### 安装教程

详见下面链接-http://www.tinywsn.net/wordpress/index.php/docs/manual/node/node-wbed/node-wbed-dev/

#### 使用说明

详见下面链接-http://www.tinywsn.net/wordpress/index.php/docs/manual/node/node-wbed/node-wbed-dev/

生成的版本位于
 fw-stm32l1-serial-usr/apps/tinysh/objs/app.bin
使用STLINK下载至板子中

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
