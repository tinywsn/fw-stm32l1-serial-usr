#ifndef _CHECKSUM_H
#define _CHECKSUM_H

uint8_t checksum(uint8_t rc, uint8_t *data, uint32_t len);

#endif