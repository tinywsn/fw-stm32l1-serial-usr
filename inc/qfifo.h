#ifndef _QFIFO_H
#define _QFIFO_H

typedef struct 
{
    void * buf;
    uint16_t  item_shft;
    uint16_t  item_num;  
    uint16_t  rd;  
    uint16_t  wr;  
} qfifo_t;

extern void qfifo_init(qfifo_t *fifo, void *buf, uint16_t item_size, uint16_t item_num);
extern bool qfifo_put(qfifo_t *fifo, void *item);
extern bool qfifo_get(qfifo_t *fifo, void *item);
extern uint16_t qfifo_free_num(qfifo_t *fifo);
extern uint16_t qfifo_used_num(qfifo_t *fifo);

#endif