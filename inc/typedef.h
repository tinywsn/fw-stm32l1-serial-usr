#ifndef _TYPEDEF_H
#define _TYPEDEF_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <float.h>

#define STX  (0x7D)
#define ETX  (0x7E)
#define DLE  (0x10)
#define ESC(c) ((c) ^ 0x20)
#define IS_NEED_ESC(c)  (((c) == STX) || ((c) == ETX) || ((c) == DLE))

// msg type
#define WNET_MSG_IPC_DATA (0x1c)

// irq priority
#define configLIBRARY_KERNEL_INTERRUPT_PRIORITY (0xff)

/// ASCII characters
#define CHAR_CR ('\r')
#define CHAR_LF ('\n')
#define CHAR_BS (0x08)
#define CHAR_DEL (0x7F)

/* GLOBAL MACROS ************************************************************/
#define NO  (0)
#define YES (1)

#define FALSE (0)
#define TRUE  (1)

#define CLEAR (0)
#define BSET  (1)

#define ERROR (-1)
#define OK     (0)


#endif
