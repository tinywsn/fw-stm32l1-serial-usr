#ifndef _EVENT_H
#define _EVENT_H

typedef enum{
  EVT_TIM,
  EVT_CHR,
  EVT_NUM,
}event_t;

typedef struct{
  event_t event;
  uint8_t data;
}msg_t;

#endif