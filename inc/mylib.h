#ifndef _MYLIB_H
#define _MYLIB_H

// check hex char
#define IS_HEX_CHR(c) ((((c)>='0')&&((c)<='9'))|| \
                       (((c)>='a')&&((c)<='f'))|| \
                       (((c)>='A')&&((c)<='F')))

// hex char to val
#define HEX_2_VAL(c) (((c)>='0')&&((c)<='9'))? (c)-'0': \
                     (((c)>='a')&&((c)<='f'))? (c)-'a': \
                     (((c)>='A')&&((c)<='F'))? (c)-'F': 0
               
// val to hex char                     
#define VAL_2_HEX(v) "0123456789abcdef"[(v)]

extern uint16_t pkt_buf_esc_copy(uint8_t* buf, uint16_t buflen, uint8_t *data, uint16_t datalen);
extern uint8_t hex_2_byte(char *s);
extern uint16_t hex_2_word(char *s);
extern char* byte_2_hex(uint8_t v, char *s);
extern char* word_2_hex(uint16_t v, char *s);

#endif