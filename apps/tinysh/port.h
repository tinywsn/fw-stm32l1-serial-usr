#ifndef _PORT_H
#define _PORT_H

#define USARTx_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
#define DBG_LED_GPIO_CLK_ENABLE()        __HAL_RCC_GPIOB_CLK_ENABLE()
#define STA_INP_GPIO_CLK_ENABLE()        __HAL_RCC_GPIOB_CLK_ENABLE()

#define USARTx_TX_PIN                    GPIO_PIN_9
#define USARTx_TX_PORT                   GPIOA
#define USARTx_TX_AF                     GPIO_AF7_USART1

#define USARTx_RX_PIN                    GPIO_PIN_10
#define USARTx_RX_PORT                   GPIOA
#define USARTx_RX_AF                     GPIO_AF7_USART1

#define DBG_LED_PIN                      GPIO_PIN_1
#define DBG_LED_PORT                     GPIOB

#define STA_INP_PIN                      GPIO_PIN_15
#define STA_INP_PORT                     GPIOB

void port_init(void);
#endif