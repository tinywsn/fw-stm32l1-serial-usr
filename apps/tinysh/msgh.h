#ifndef _MSGH_H
#define _MSGH_H

#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "config.h"
#include "event.h"

void rx_msg(msg_t *pmsg);
#endif
