#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "config.h"
#include "main.h"
#include "pkth.h"
#include "port.h"
#include "serial.h"
#include "checksum.h"
#include "mylib.h"

// pkt proc
void rx_pkt(wipc_pkt_hdr_t *hdr, uint8_t len)
{
  uint8_t c, o, *p, *buf, mlen = len - sizeof(wipc_pkt_hdr_t);
  mt_data_t *pd = &mt_data;
  
  p = (uint8_t*)hdr + sizeof(wipc_pkt_hdr_t);
  c = *p++; o = *p++;

  // set
  if((mlen == 3) && (c == 's') && (o == 'l') && IS_HEX_CHR(*p)){
    pd->led = HEX_2_VAL(*p);
    HAL_GPIO_WritePin(DBG_LED_PORT, DBG_LED_PIN, pd->led? GPIO_PIN_RESET: GPIO_PIN_SET);
    return;
  }
  // qry
  if((mlen == 2) && (c == 'q') && (o == 'l')){
    p = buf = serial_tx_esc;
    // pkt hdr
    ((wipc_pkt_hdr_t*)p)->len = sizeof(wipc_pkt_hdr_t)+3;
    ((wipc_pkt_hdr_t*)p)->dst = ((wipc_pkt_hdr_t*)p)->dst2 = WIPC_GTWAY_ADDR;
    ((wipc_pkt_hdr_t*)p)->src = ((wipc_pkt_hdr_t*)p)->src2 = WIPC_LOCAL_ADDR;
    ((wipc_pkt_hdr_t*)p)->ctrl = WIPC_C_UP;
    ((wipc_pkt_hdr_t*)p)->cksum = 0;
    ((wipc_pkt_hdr_t*)p)->type = WIPC_PKT_USR_DATA;
    p += sizeof(wipc_pkt_hdr_t);
    // msg data
    *p++ = 'r';
    *p++ = 'l';
    *p++ = VAL_2_HEX(pd->led);
    // cksum
    p = buf;
    ((wipc_pkt_hdr_t*)p)->cksum = 0 - checksum(0, p, ((wipc_pkt_hdr_t*)p)->len);
    serial_txpkt(p, ((wipc_pkt_hdr_t*)p)->len);
    return;
  }
}

