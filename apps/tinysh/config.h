#ifndef _CONFIG_H
#define _CONFIG_H

// msg num
#define CFG_MSG_FIFO_BUF_NUM (128)

// rx pkt
#define CFG_RX_PKT_BUF_SIZE (128)

// serial setting
#define CFG_SERIAL_BAUD_RATE (115200UL)
#define CFG_SERIAL_TX_BUF_SIZE (128)
#define CFG_ESC_BUF_OFT (16)
#define CFG_SERIAL_ESC_BUF_SIZE (CFG_SERIAL_TX_BUF_SIZE - CFG_ESC_BUF_OFT)

#endif
