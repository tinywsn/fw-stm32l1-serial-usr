#ifndef _PKTH_H
#define _PKTH_H
#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "config.h"
#include "wipc.h"

void rx_pkt(wipc_pkt_hdr_t *hdr, uint8_t len);
#endif
