#ifndef _MAIN_H
#define _MAIN_H

#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "event.h"
#include "qfifo.h"

#define MT_F_NONE (0x00)
#define MT_F_ESC (0x01)

typedef struct{
  uint8_t idx;
  uint8_t flag;
  uint8_t led;
  uint8_t rx_buf[CFG_RX_PKT_BUF_SIZE];  
  uint16_t rx_idx;
  uint16_t rx_err;
  uint32_t ticks;
}mt_data_t;

extern mt_data_t mt_data;
extern qfifo_t msg_fifo;
#endif
