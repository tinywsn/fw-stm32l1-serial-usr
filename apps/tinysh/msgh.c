#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "config.h"
#include "checksum.h"
#include "main.h"
#include "msgh.h"
#include "pkth.h"
#include "port.h"
#include "serial.h"
#include "wipc.h"
#include "mylib.h"

// evt chr
void evt_chr_hdl(uint8_t d)
{
  mt_data_t *pd = &mt_data;
  uint8_t c = d;

  if(c == STX){
    pd->flag = MT_F_NONE;
    pd->rx_idx = 0;
    return;
  }
  if(c == DLE){
    if(pd->flag & MT_F_ESC)
      return;
    pd->flag |= MT_F_ESC;
    return;
  }
  if(pd->flag & MT_F_ESC){
    pd->flag &=~MT_F_ESC;
    d = ESC(c);
  } 
  if(c == ETX){
    if((pd->rx_idx == pd->rx_buf[0]) && (!checksum(0, pd->rx_buf, pd->rx_idx))){
      // rx pkt 
      rx_pkt((wipc_pkt_hdr_t*)pd->rx_buf, pd->rx_idx);
    }else{
      pd->rx_err++;
    }
    return;
  }
  pd->rx_buf[pd->rx_idx++] = d;
  if(pd->rx_idx >= CFG_RX_PKT_BUF_SIZE){
    pd->rx_idx--;
  }
}

// evt tim  
void evt_tim_hdl(uint8_t data)
{
  mt_data_t *pd = &mt_data;
  uint8_t *p, *buf;

  pd->ticks++;
  // dbg led
  if((pd->ticks & 0x07ff) == 0x0000){
     HAL_GPIO_WritePin(DBG_LED_PORT, DBG_LED_PIN, GPIO_PIN_RESET);
  }else if((pd->ticks & 0x07ff) == 0x0100){
    HAL_GPIO_WritePin(DBG_LED_PORT, DBG_LED_PIN, GPIO_PIN_SET);
  }    
  // cks sta
  if(HAL_GPIO_ReadPin(STA_INP_PORT, STA_INP_PIN) == GPIO_PIN_SET)
    return;
  // tx pkt
  p = buf = serial_tx_esc;
  // pkt hdr
  ((wipc_pkt_hdr_t*)p)->len = sizeof(wipc_pkt_hdr_t)+4;
  ((wipc_pkt_hdr_t*)p)->dst = ((wipc_pkt_hdr_t*)p)->dst2 = WIPC_GTWAY_ADDR;
  ((wipc_pkt_hdr_t*)p)->src = ((wipc_pkt_hdr_t*)p)->src2 = WIPC_LOCAL_ADDR;
  ((wipc_pkt_hdr_t*)p)->ctrl = WIPC_C_UP|WIPC_C_UI;
  ((wipc_pkt_hdr_t*)p)->cksum = 0;
  ((wipc_pkt_hdr_t*)p)->type = WIPC_PKT_USR_DATA;
  p += sizeof(wipc_pkt_hdr_t);
  // msg data
  *p++ = 'n';
  *p++ = 't';
  p = (uint8_t*)byte_2_hex(23 + (pd->idx++ & 0x07), (char*)p);
  // cksum
  p = buf;
  ((wipc_pkt_hdr_t*)p)->cksum = 0 - checksum(0, p, ((wipc_pkt_hdr_t*)p)->len);
  serial_txpkt(p, ((wipc_pkt_hdr_t*)p)->len);
}

// msg proc
void rx_msg(msg_t *pmsg)
{
  switch(pmsg->event){
  case EVT_TIM:
    evt_tim_hdl(pmsg->data);
    break;
  case EVT_CHR:
    evt_chr_hdl(pmsg->data);
    break;
  default:
    break;  
  }
}

