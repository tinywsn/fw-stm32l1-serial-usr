#include "stm32l1xx_hal.h"

uint8_t checksum(uint8_t rc, uint8_t *data, uint32_t len)
{
  for(uint32_t i = 0; i < len; i++){
    rc += *data++;
  }
  return rc;
}
