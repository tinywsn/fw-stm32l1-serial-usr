#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "config.h"
#include "error.h"

int __errno = 0;
void fatal(char *p)
{
  for(;;){
    NVIC_SystemReset();
  }
}
