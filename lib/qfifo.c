#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "config.h"
#include "error.h"
#include "qfifo.h"

/* init fifo */
void qfifo_init(qfifo_t *fifo, void *buf, uint16_t item_size, uint16_t item_num)
{
  fifo->buf = buf;
  fifo->item_num = item_num;
  fifo->rd = fifo->wr = 0;
  // find size shft
  for(fifo->item_shft = 0; fifo->item_shft < 8; fifo->item_shft++){
    if((1u << fifo->item_shft) == item_size){
      break;
    }
  }
  if(fifo->item_shft >= 8) fatal("qfifo init fail");
}

/* put item to tail */
bool qfifo_put(qfifo_t *fifo, void *item)
{
  uint16_t i;
  uint8_t *s, *d;
  
  if(qfifo_free_num(fifo) <= 0) 
    return false;
  d = (uint8_t*)fifo->buf + (fifo->wr << fifo->item_shft);
  s = (uint8_t*)item;
  for(i = 0; i < (1u << fifo->item_shft); i++){
    *d++ = *s++;
  }
  if(++fifo->wr >= fifo->item_num){
    fifo->wr = 0;
  }
  return true;
}

/* get head item */
bool qfifo_get(qfifo_t *fifo, void *item)
{
  uint16_t i;
  uint8_t *s, *d;
  
  if(qfifo_used_num(fifo) <= 0) 
    return false;
  s = (uint8_t*)fifo->buf + (fifo->rd << fifo->item_shft);
  d = (uint8_t*)item;
  for(i = 0; i < (1u << fifo->item_shft); i++){
    *d++ = *s++;
  }
  if(++fifo->rd >= fifo->item_num){
    fifo->rd = 0;
  }
  return true;
}

/* return used num */
uint16_t qfifo_used_num(qfifo_t *fifo)
{
  if(fifo->wr >= fifo->rd){
    return fifo->wr - fifo->rd;
  }else{
    return fifo->wr + fifo->item_num - fifo->rd;
  }
}

/* return free num */
uint16_t qfifo_free_num(qfifo_t *fifo)
{
  if(fifo->rd > fifo->wr){
    return fifo->rd - fifo->wr - 1;
  }else{
    return fifo->rd + fifo->item_num - fifo->wr - 1;
  }
}
