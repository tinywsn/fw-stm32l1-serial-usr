#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "config.h"
#include "error.h"
#include "mylib.h"

// esc copy
uint16_t pkt_buf_esc_copy(uint8_t* buf, uint16_t buflen, uint8_t *data, uint16_t datalen)
{
  uint16_t i, j;
  uint8_t c;
  
  // first STX
  j = 0;
  buf[j++]= STX;
  // ESC info
  for(i=0; i<datalen; i++){
    c = data[i];
    if(IS_NEED_ESC(c)){
      buf[j++]= DLE;
      c = ESC(c);
    }
    buf[j++]= c;        
  }
  // last ETX
  buf[j++]= ETX;
  // check overflow
  if(j >= buflen)
    fatal("esc buf overflow");
  return j;
}

// hex str to byte
uint8_t hex_2_byte(char *s)
{
  uint8_t v;
  v = HEX_2_VAL(*s++);
  v = (v << 4) | HEX_2_VAL(*s++);
  return v;
}

// byte to hex str
char* byte_2_hex(uint8_t v, char *s)
{
  *s++ = VAL_2_HEX((v >> 4) & 0x0f);
  *s++ = VAL_2_HEX(v & 0x0f);
  return s;
}

// hex str to word
uint16_t hex_2_word(char *s)
{
  uint16_t v;
  v = HEX_2_VAL(*s++);
  v = (v << 4) | HEX_2_VAL(*s++);
  v = (v << 4) | HEX_2_VAL(*s++);
  v = (v << 4) | HEX_2_VAL(*s++);
  return v;
}

// word to hex str
char* word_2_hex(uint16_t v, char *s)
{
  *s++ = VAL_2_HEX((v >> 12) & 0x0f);
  *s++ = VAL_2_HEX((v >> 8) & 0x0f);  
  *s++ = VAL_2_HEX((v >> 4) & 0x0f);
  *s++ = VAL_2_HEX(v & 0x0f);
  return s;
}
