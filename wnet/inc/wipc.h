/*
 * Copyright (c) 2021, www.tinywsn.net
 */

#ifndef _WIPC_H
#define _WIPC_H

// wnet packet frame notes:
//   byte order little-endian
//   float IEEE 754 format

#if defined(__ICC430__)
  #pragma pack(1)
  #define __attribute__(...)
#elif defined(__ICCSTM8__)
  #define __attribute__(...)
#elif defined(__ICCARM__)
  #pragma pack(1)
  #define __attribute__(...)
#elif defined(__GNUC__)
#else
  #error toolchain not support
#endif

// vtab oft
#define WIPC_NET_MSG_PROC_VTAB (0x08002144)
#define WIPC_USR_MSG_PROC_VTAB (0x08010008)

// ctx size
#define WIPC_CTX_BUF_SIZE (384)

// nvram size
#define WIPC_NVM_DATA_SIZE (16)

// data size
#define WIPC_USR_DATA_SIZE (41)

// addr size
#define WIPC_PKT_ADDR_SIZE (4)

// host addr
#define WIPC_LOCAL_ADDR (0x7f000001)
#define WIPC_GTWAY_ADDR (0x00000001)
#define WIPC_BCAST_ADDR (0xffffffff)

// pkt type
#define WIPC_PKT_USR_DATA (0x1b)
#define WIPC_PKT_RSP_STAT (0x1c)

// stat
#define WIPC_S_NONE (0x00)
#define WIPC_S_CONN (0x01)
#define WIPC_S_RXQF (0x02)
#define WIPC_S_RXNE (0x04)

// dir mask
#define WIPC_C_DIR (0x8000)

// ctrl bit
#define WIPC_C_UP (0x8000)
#define WIPC_C_CMD (0x1000)
#define WIPC_C_UI (0x0400)

#define WIPC_C_FWD (0x0004)
#define WIPC_C_BCAST (0x0002)

// cks dir
#define IF_WIPC_PKT_UP(ctrl) (((ctrl) & WIPC_C_DIR) == WIPC_C_UP)
#define IF_WIPC_PKT_DN(ctrl) (((ctrl) & WIPC_C_DIR) != WIPC_C_UP)

// arch
typedef enum{
  WIPC_ARCH_CC430F5137,
  WIPC_ARCH_MSP430F5438A,
  WIPC_ARCH_STM8L151,
  WIPC_ARCH_STM32L151,
  WIPC_ARCH_STM32L053,  
  WIPC_ARCH_NUM,
}wipc_arch_t;

// mrfi
typedef enum{
  WIPC_MRFI_CC430,
  WIPC_MRFI_CC110X,
  WIPC_MRFI_SI446X,
  WIPC_MRFI_SX127X,
  WIPC_MRFI_SX126X,
  WIPC_MRFI_NUM,
}wipc_mrfi_t;

// func
typedef enum{
  WIPC_FUNC_NOD,
  WIPC_FUNC_RTB,
  WIPC_FUNC_RTT,
  WIPC_FUNC_RTU,
  WIPC_FUNC_USR,
  WIPC_FUNC_LDR,
  WIPC_FUNC_NUM,
}wipc_func_t;

// result
typedef enum{
  WIPC_RES_SUCC,
  WIPC_RES_FAIL,
  WIPC_RES_CONT,
  WIPC_RES_NUM,
}wipc_result_t;

// dir
typedef enum{
  WIPC_DIR_UP,
  WIPC_DIR_DN,
  WIPC_DIR_NUM,
}wipc_dir_t;

// msg type
typedef enum{
  WIPC_MSG_SYS_INIT,
  WIPC_MSG_SYS_EXIT,
  WIPC_MSG_SYS_RST,
  WIPC_MSG_LPM_REQ,
  WIPC_MSG_LPM_INIT,
  WIPC_MSG_LPM_EXIT,
  WIPC_MSG_LPM_HALT,
  WIPC_MSG_LPM_CONT,
  WIPC_MSG_NET_CONN,
  WIPC_MSG_NET_DISC,
  WIPC_MSG_NET_HALT,
  WIPC_MSG_NET_CONT,
  WIPC_MSG_TMR_INIT,  
  WIPC_MSG_TMR_HALT,    
  WIPC_MSG_TMR_TICK,
  WIPC_MSG_QRY_TICK,  
  WIPC_MSG_SYS_TICK,
  WIPC_MSG_NTS_INIT,
  WIPC_MSG_NTS_DONE,
  WIPC_MSG_BTS_INIT,
  WIPC_MSG_BTS_DONE,
  WIPC_MSG_USR_DATA,
  WIPC_MSG_QRY_STAT,
  WIPC_MSG_RSP_STAT,
  WIPC_MSG_EVT_SEND,
  WIPC_MSG_EVT_RECV,
  WIPC_MSG_EVT_FIND,
  WIPC_MSG_IRQ_VTOR,
  WIPC_MSG_IRQ_DIS,
  WIPC_MSG_IRQ_ENA,
  WIPC_MSG_NVM_SAVE,
  WIPC_MSG_NVM_LOAD,
  WIPC_MSG_LOG_TEXT,
  WIPC_MSG_TXQ_RST,
  WIPC_MSG_SLP_USEC,
  WIPC_MSG_NUM,
}wipc_msg_type_t;

// pkt hdr
typedef struct{
  uint8_t len;
  uint32_t dst;
  uint32_t src;
  uint32_t dst2;
  uint32_t src2;
  uint16_t ctrl;
  uint8_t cksum;
  uint8_t type;
} __attribute__((packed)) wipc_pkt_hdr_t;

// WIPC_MSG_SYS_INIT
typedef struct{
  uint8_t stat;
}__attribute__((packed)) wipc_msg_sys_init_t;

// tmr type
typedef enum{
  WIPC_TMR_SYNC,
  WIPC_TMR_ASYN,
}wipc_tmr_t;

// WIPC_MSG_TMR_INIT
typedef struct{
  uint8_t type;
  union{
    // sync  
    int8_t oft;
    // asyn
    uint32_t val;
  }arg;
}__attribute__((packed)) wipc_msg_tmr_init_t;

// WIPC_MSG_TMR_HALT
typedef struct{
  uint8_t type;
}__attribute__((packed)) wipc_msg_tmr_halt_t;

// WIPC_MSG_TMR_TICK
typedef struct{
  uint8_t type;
}__attribute__((packed)) wipc_msg_tmr_tick_t;

// WIPC_MSG_QRY_TICK
typedef struct{
  uint32_t count;
}__attribute__((packed)) wipc_msg_sys_tick_t;

// WIPC_MSG_USR_DATA
typedef struct{
  uint8_t data[1]; // 1..WIPC_PKT_DATA_SIZE
}__attribute__((packed)) wipc_msg_usr_data_t;

// WIPC_MSG_RSP_STAT
typedef struct{
  uint8_t stat;
}__attribute__((packed)) wipc_msg_rsp_stat_t;

// WIPC_MSG_EVT_SEND
// WIPC_MSG_EVT_RECV
typedef struct{
  uint8_t evt;
  uint32_t data;
}__attribute__((packed)) wipc_msg_evt_data_t;

// WIPC_MSG_IRQ_VTOR
typedef struct{
  uint16_t vtor;
}__attribute__((packed)) wipc_msg_irq_data_t;

// WIPC_MSG_NVM_SAVE
// WIPC_MSG_NVM_LOAD
typedef struct{
  uint8_t data[WIPC_NVM_DATA_SIZE];
}__attribute__((packed)) wipc_msg_nvm_data_t;

// WIPC_MSG_TXQ_RST
typedef struct{
  uint8_t all;
}__attribute__((packed)) wipc_msg_txq_rst_t;

// WIPC_MSG_SLP_USEC
typedef struct{
  uint16_t usec;
}__attribute__((packed)) wipc_msg_slp_usec_t;

// msg proc prototype 
typedef wipc_result_t (*wipc_msg_proc_t)(wipc_msg_type_t msg, void *in,  void *out, void *ctx);

#if defined(__ICC430__)
  #pragma pack()
#elif defined(__ICCSTM8__)
#elif defined(__ICCARM__)
  #pragma pack()
#elif defined(__GNUC__)
#endif

#endif