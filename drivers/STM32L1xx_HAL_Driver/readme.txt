remove HAL_SPI_Init in stm32l1xx_hal_spi.c 
------------------------------------------------------------------------
__weak HAL_StatusTypeDef HAL_SPI_Init(SPI_HandleTypeDef *hspi)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(hspi);

  return HAL_ERROR;
}
