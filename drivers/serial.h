#ifndef _SERIAL_H
#define _SERIAL_H

#define USARTx  USART1
#define USARTx_IRQn  USART1_IRQn 
#define __HAL_RCC_USARTx_CLK_ENABLE  __HAL_RCC_USART1_CLK_ENABLE

extern uint8_t serial_tx_buf[];
extern uint8_t *serial_tx_esc;

void serial_init(void);
uint16_t serial_send(uint8_t *buf, uint16_t size);
uint16_t serial_txpkt(uint8_t *buf, uint16_t size);
uint16_t serial_puts(char *s);

#endif
