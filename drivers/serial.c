#include <string.h>
#include "stm32l1xx_hal.h"
#include "typedef.h"
#include "config.h"
#include "error.h"
#include "mylib.h"
#include "qfifo.h"
#include "event.h"
#include "serial.h"
#include "main.h"

static UART_HandleTypeDef uart_handle;
uint8_t serial_tx_buf[CFG_SERIAL_TX_BUF_SIZE];
uint8_t *serial_tx_esc = serial_tx_buf + CFG_ESC_BUF_OFT;

// init
void serial_init(void)
{
  /* Enable USART clock */
  __HAL_RCC_USARTx_CLK_ENABLE();
  
  /* USART configuration */
  uart_handle.Instance        = USARTx;
  uart_handle.Init.BaudRate   = CFG_SERIAL_BAUD_RATE;
  uart_handle.Init.WordLength = UART_WORDLENGTH_8B;
  uart_handle.Init.StopBits   = UART_STOPBITS_1;
  uart_handle.Init.Parity     = UART_PARITY_NONE;
  uart_handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
  uart_handle.Init.Mode       = UART_MODE_TX_RX;
  uart_handle.Init.OverSampling = UART_OVERSAMPLING_16;
  HAL_UART_Init(&uart_handle);
     
  /* Enable the USART Receive interrupt: this interrupt is generated when the USART
     receive data register is not empty */
  HAL_NVIC_SetPriority(USARTx_IRQn, configLIBRARY_KERNEL_INTERRUPT_PRIORITY, 0);
  HAL_NVIC_EnableIRQ(USARTx_IRQn);

  /* Enable the UART Data Register not empty Interrupt */
  __HAL_UART_ENABLE_IT(&uart_handle, UART_IT_RXNE);
}

// send
uint16_t serial_send(uint8_t *buf, uint16_t size)
{
  for(int i = 0; i < size; i++){
    while(!(uart_handle.Instance->SR & USART_SR_TC));
    uart_handle.Instance->DR = buf[i];        
  }
  return size;
}

// txpkt
uint16_t serial_txpkt(uint8_t *buf, uint16_t size)
{
  uint16_t s;
  
  s = pkt_buf_esc_copy(serial_tx_buf, CFG_SERIAL_TX_BUF_SIZE, (uint8_t*)buf, size);
  if((s - size) >= CFG_ESC_BUF_OFT) fatal("esc buf overlay");    
  
  return serial_send(serial_tx_buf, s);
}

// puts
uint16_t serial_puts(char *s)
{
  return serial_send((uint8_t*)s, strlen(s));
}

// irq
void USART1_IRQHandler(void)
{
  uint32_t status = uart_handle.Instance->SR;
  msg_t msg;

  // rx  
  if(status & (UART_FLAG_RXNE|UART_FLAG_ORE)){
    __HAL_UART_CLEAR_FLAG(&uart_handle, UART_FLAG_RXNE);
    __HAL_UART_CLEAR_FLAG(&uart_handle, UART_FLAG_ORE);
    // notify main thread
    msg.event = EVT_CHR;
    msg.data = uart_handle.Instance->DR;
    qfifo_put(&msg_fifo, &msg);
  } 
}

